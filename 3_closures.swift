// Closure

// Сортировка массива с помощью замыкания
var numbers = [5, 2, 8, 1, 7, 3]

// Сортировка в одну сторону
numbers.sort { $0 < $1 } 
print(numbers)

// Сортировка в обратную сторону
numbers.sort { $0 > $1 } 
print(numbers)

// -----

// Создать метод, который принимает имена друзей, после этого имена положить в массив
func addFriends(_ names: String...) -> [String] {
    var friends = [String]()
    for name in names {
        friends.append(name)
    }
    return friends
}
var friends = addFriends("Noname", "Alex", "Nastya" ,"Dino")
print(friends)


// Массив отсортировать по количеству букв в имени
friends.sort { $0.count < $1.count }
print(friends)

// -----

// Создать словарь (Dictionary), где ключ - кол-во символов в имни, а в значении - имя друга
// А если одинаковое кол-во символов в именах? 
//(╮°-°)╮┳━━┳ ( ╯°□°)╯ ┻━━┻         
var friendsDict = [Int: String]()

for friend in friends {
    friendsDict[friend.count] = friend
}
print(friendsDict)

//Написать функцию, которая будет принимать ключ, выводить полученный ключ и значение

func printFriend(_ key: Int) {
    if let friend = friendsDict[key] {
        print("Friend '\(friend)' with key \(key)")
    } else {
        print("No friend found with key \(key)")
    }
}

printFriend(4)

// -----

//Написать функцию, которая принимает 2 массива (один строковый, второй - числовой) и проверяет их на пустоту: если пустой - то добавьте любое значения и выведите массив в консоль

func isEmptyArrays(strArr: [String], intArr: [Int]) {
   if strArr.isEmpty {
        var strArrNew = strArr
        strArrNew.append("value")
        print("The new str array is: \(strArrNew)")
    } else {
        print("The string array is not empty")
    }
    
    if intArr.isEmpty {
        var intArrNew = intArr
        intArrNew.append(0)
        print("The new int array is: \(intArrNew)")
    } else {
        print("The integer array is not empty")
    }
}

var strArray: [String] = []
var intArray = [6, 6, 5]
isEmptyArrays(strArr: strArray, intArr: intArray)
