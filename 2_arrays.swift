let days = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

// -----

let month = ["январь", "февраль", "март", "апрель", "май", "июнь", "июль", "август", "сентябрь", "октябрь", "ноябрь", "декабрь"]

// -----

for day in 0..<days.count {
    print("\(month[day]): \(days[day])")
}

// -----

var tupleArray = [(month: String, days: Int)]()
for i in 0..<month.count {
    tupleArray.append((month[i], days[i]))
}

// -----

var monthCount = month.count - 1

for i in 0..<month.count {
    print("\(month[(i + monthCount)]) = \(days[i + monthCount]) days")
    monthCount -= 2
}

// -----

let date = (month: 10, day: 12)
 
var sumDays = 0
 
for month in 0..<(date.month - 1) {
    sumDays += days[month]
}
sumDays += date.day
 
print("Days left = \(sumDays)")