// Protocols

// Реализовать структуру IOSCollection и создать в ней copy on write по типу
func address(of object: UnsafeRawPointer) -> Void {
  let addr = Int(bitPattern: object)
  print(addr)
}

func address(off value: AnyObject) -> Void {
  print("\(Unmanaged.passUnretained(value).toOpaque())")
}

struct IOSCollection {
  var type = "array"
}

class Ref<T> {
  var value: T
  init(value: T) {
    self.value = value
  }
}

struct Container<T> {
  var ref: Ref<T>
  init(value: T) {
    self.ref = Ref(value: value)
  }

  var value: T {
    get {
      return ref.value
    }
    set {
      guard (isKnownUniquelyReferenced(&ref)) else {
        ref = Ref(value: newValue)
        return
      } 
      ref.value = newValue
    }
 }
}


var type = IOSCollection()
var container1 = Container(value: type)
var container2 = container1

address(off: container1.ref)
address(off: container2.ref)

container2.value.type = "list"

address(off: container1.ref)
address(off: container2.ref)

// ----

// Создать протокол *Hotel* с инициализатором, который принимает roomCount, после создать class HotelAlfa добавить свойство roomCount и подписаться на этот протокол
protocol Hotel {
    var roomCount: Int { get }
    
    init(roomCount: Int)
}
    
 class HotelAlfa: Hotel {
    var roomCount: Int
    
    required init(roomCount: Int) {
        self.roomCount = roomCount
    }
}

// ----

// Создать protocol GameDice у него {get} свойство numberDice далее нужно расширить Int так, чтобы когда мы напишем такую конструкцию 'let diceCoub = 4 diceCoub.numberDice' в консоле мы увидели такую строку - 'Выпало 4 на кубике'
protocol GameDice {
    var numberDice: Int { get }
}

extension Int: GameDice {
    var numberDice: Int {
      print("Выпало \(self) на кубике")
      return self     
    }
}

let diceCoub = 4
print(diceCoub.numberDice)

// ----

// Создать протокол с одним методом и 2 свойствами одно из них сделать явно optional, создать класс, подписать на протокол и реализовать только 1 обязательное свойство
protocol MyProtocol {
    var requiredProperty: Int { get }
    var optionalProperty: String? { get set }
    
    func myMethod()
}

class MyClass: MyProtocol {
    var requiredProperty: Int = 0
    var optionalProperty: String?
    
    func myMethod() {
        print(requiredProperty)
    }
}

// ----

// Создать 2 протокола: со свойствами время, количество кода и функцией writeCode(platform: Platform, numberOfSpecialist: Int); и другой с функцией: stopCoding(). Создайте класс: Компания, у которого есть свойства - количество программистов, специализации(ios, android, web)
// Компании подключаем два этих протокола
enum Platform: String {
  case ios
  case android
  case web
}

protocol CodingProtocol {
    var time: Double { get set }
    var codeLines: Int { get set }
    func writeCode(platform: Platform)
}

protocol StopCodingProtocol {
    func stopCoding()
}

class Company: CodingProtocol, StopCodingProtocol {
    var numberOfProgrammers: Int
    var specialties: [Platform]
    var time: Double = 0
    var codeLines: Int = 0
    
    init(numberOfProgrammers: Int, specialties: [Platform]) {
        self.numberOfProgrammers = max(numberOfProgrammers, 0) // обрабатываем крайний случай, когда количество программистов меньше нуля
        self.specialties = specialties
    }
    
    func writeCode(platform: Platform) {
        if (numberOfProgrammers == 0) {
            print("Разработка не может быть начата. Кол-во специалистов равно 0.")
            return
        }
        print("Разработка началась. Пишем код для \(platform.rawValue) с помощью \(numberOfProgrammers) специалистов.")        
        time += 1.5 
        codeLines += numberOfProgrammers * Int(time)
    }
    
    func stopCoding() {        
        print("Разработка окончена. Сдаю в тестирование. Время: \(time) часов. Количество строк кода: \(codeLines).")
    }
}

let company = Company(numberOfProgrammers: 3, specialties: [.ios, .android, .web])
company.writeCode(platform: .ios)
company.stopCoding()