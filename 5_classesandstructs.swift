// Classes and Structs

// Создать класс родитель и 2 класса наследника
class Person {
  var name: String
  var age: Int

  init(name: String, age: Int) {
    self.name = name
    self.age = age
  }
}

class Student: Person {}
class Teacher: Person {}

// ----

// Создать класс *House* в нем несколько свойств - *width*, *height* и несколько методов: *create*(выводит площадь),*destroy*(отображает что дом уничтожен)
class House {
  var width: Double
  var height: Double
  
  init(width: Double, height: Double) {
    self.width = width
    self.height = height
  }
  
  func create() {
    print("S = \(width * height)")
  }
  
  func destroy() {
    print("The house has been destroyed.")
  }
}

let myHouse = House(width: 10.0, height: 5.0)
myHouse.create()
myHouse.destroy()

// ----

// Создайте класс с методами, которые сортируют массив учеников по разным параметрам

class StudentSorter {
  
  func sortByName(_ students: [Student]) -> [Student] {
    return students.sorted { $0.name < $1.name }
  }
  
  func sortByAge(_ students: [Student]) -> [Student] {
    return students.sorted { $0.age < $1.age }
  }  
}

func printStudents(_ students: [Student]) {
  for student in students {
    print("Name: \(student.name), Age: \(student.age)")
  }
}
let studentsArray: [Student] = [Student(name: "Alex", age: 19), Student(name: "Dima", age: 18)]
let sorter = StudentSorter()
let sortedByName = sorter.sortByName(studentsArray)
printStudents(sortedByName)
let sortedByAge = sorter.sortByAge(studentsArray)
printStudents(sortedByAge)

// ----

// Написать свою структуру и класс, и пояснить в комментариях - чем отличаются структуры от классов

struct SomeStructure {
    var data: String = "Data"
}

class SomeClass {
    var data: String = "Data"
}
// Отличия структур от классов:
// - Структуры передаются по значению, а классы - по ссылке
// - Классы могут наследоваться от других классов, а структуры не могут наследовать от других структур
// - У классов есть деинициализаторы, у структур нет
// - У классов есть приведение типов, которое позволяет проверить и интерпретировать тип экземпляра класса в процессе выполнения

// ----
 
// Напишите простую программу, которая отбирает комбинации в покере из рандомно выбранных 5 карт

func generateCards() -> [(String, String)] {
  let suits = ["♠️", "♥️", "♦️", "♣️"]
  let values = ["2", "3", "4", "5", "6", "7", "8", "9",   "10", "J", "Q", "K", "A"]

  // Сохраняйте комбинации в массив
  var cards: [(String, String)] = []
  while cards.count < 5 {
      let randomSuit = suits.randomElement()!
      let randomValue = values.randomElement()!
      let newCard = (randomSuit, randomValue)
      if !cards.contains(where: { $0 == newCard }) {
          cards.append(newCard)
      }
  } 
  return cards
}

func findPokerHand(cards: [(String, String)]) -> String {
  // Отсортируйте карты по номиналу
  let sortedCards = cards.sorted { (card1, card2) -> Bool in
    let value1 = getValue(for: card1.1)
    let value2 = getValue(for: card2.1)
    return value1 < value2
  }
  
  // Проверьте наличие каждой из комбинаций
  if isRoyalFlush(cards: sortedCards) {
    return "Роял Флеш"
  } else if isStraightFlush(cards: sortedCards) {
    return "Стрит Флеш"
  } else if isFourOfAKind(cards: sortedCards) {
    return "Каре"
  } else if isFullHouse(cards: sortedCards) {
    return "Фул хаус"
  } else if isFlush(cards: sortedCards) {
    return "Флеш"
  } else if isStraight(cards: sortedCards) {
    return "Стрит"
  } else if isThreeOfAKind(cards: sortedCards) {
    return "Сет"
  } else if isTwoPair(cards: sortedCards) {
    return "Две пары"
  } else if isPair(cards: sortedCards) {
    return "Пара"
  } else {
    return "Старшая карта"
  }
}

// Помощник для получения числового значения карты
func getValue(for card: String) -> Int {
  switch card {
  case "A":
    return 14
  case "K":
    return 13
  case "Q":
    return 12
  case "J":
    return 11
  default:
    return Int(card) ?? 0
  }
}

// Проверьте, является ли комбинация Royal Flush
func isRoyalFlush(cards: [(String, String)]) -> Bool {
  let suit = cards[0].0
  let values = ["10", "J", "Q", "K", "A"]
  for i in 0..<5 {
    if cards[i].0 != suit || cards[i].1 != values[i] {
      return false
    }
  }
  return true
}

// Проверьте, является ли комбинация Straight Flush
func isStraightFlush(cards: [(String, String)]) -> Bool {
  return isStraight(cards: cards) && isFlush(cards: cards)
}

// Проверьте, является ли комбинация Four of a Kind
func isFourOfAKind(cards: [(String, String)]) -> Bool {
  for i in 0..<2 {
    if cards[i].1 == cards[i+1].1 && cards[i+1].1 == cards[i+2].1 && cards[i+2].1 == cards[i+3].1 {
      return true
    }
  }
  return false
}

// Проверьте, является ли комбинация Full House
func isFullHouse(cards: [(String, String)]) -> Bool {
  if cards[0].1 == cards[1].1 && cards[1].1 == cards[2].1 && cards[3].1 == cards[4].1 {
    return true
  } else if cards[0].1 == cards[1].1 && cards[2].1 == cards[3].1 && cards[3].1 == cards[4].1 {
    return true
  }
  return false
}

// Проверьте, является ли комбинация Flush
func isFlush(cards: [(String, String)]) -> Bool {
let suit = cards[0].0
for i in 1..<5 {
if cards[i].0 != suit {
return false
}
}
return true
}

// Проверьте, является ли комбинация Straight
func isStraight(cards: [(String, String)]) -> Bool {
  for i in 0..<4 {
    if getValue(for: cards[i].1) + 1 != getValue(for: cards[i+1].1) {
      return false
    }
  }
  return true
}

// Проверьте, является ли комбинация Three of a Kind
func isThreeOfAKind(cards: [(String, String)]) -> Bool {
  if cards[0].1 == cards[1].1 && cards[1].1 == cards[2].1 {
    return true
  } else if cards[1].1 == cards[2].1 && cards[2].1 == cards[3].1 {
    return true
  } else if cards[2].1 == cards[3].1 && cards[3].1 == cards[4].1 {
    return true
  }
  return false
}

// Проверьте, является ли комбинация Two Pair
func isTwoPair(cards: [(String, String)]) -> Bool {
if cards[0].1 == cards[1].1 && cards[2].1 == cards[3].1 {
    return true
  } else if cards[0].1 == cards[1].1 && cards[3].1 == cards[4].1 {
    return true
  } else if cards[1].1 == cards[2].1 && cards[3].1 == cards[4].1 {
    return true
  }
    return false
}

// Проверьте, является ли комбинация Pair
func isPair(cards: [(String, String)]) -> Bool {
  for i in 0..<4 {
    if cards[i].1 == cards[i+1].1 {
      return true
    }
  }
  return false
}

var cards = generateCards()

print("Карты:")
for card in cards {
    print("\(card.1) of \(card.0)")
}
// Генерируйте пять карт и найдите комбинацию
let hand = findPokerHand(cards: cards)
print("Вы получили: \(hand)")
