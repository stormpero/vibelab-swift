//Enums

// Создайте по 2 enum с разным типом RawValue
enum Direction : String {
  case Left = "Left"
  case Right = "Right"
}

enum ElementType : Int {
  case Folder = 0
  case File
}

// ----

// Создайте несколько enum для заполнения полей стркутуры - анкета сотрудника: enum пол, enum категория возраста, enum стаж

// Enum для пола
enum Gender {
    case male
    case female
}

// Enum для категории возраста
enum AgeCategory {
    case under18
    case over18
}

// Enum для стажа
enum Experience {
    case lessThan1Year
    case from1to3Years
    case from3to5Years
    case over5Years
}

struct Employee {
    var name: String
    var gender: Gender
    var ageCategory: AgeCategory  
    var experience: Experience
}

// ----

// Создать enum со всеми цветами радуги
enum RainbowColor {
    case red
    case orange
    case yellow
    case green
    case blue
    case indigo
    case violet
}

// ----

// Создать функцию, которая содержит массив разных case'ов enum и выводит содержимое в консоль. Пример результата в консоли 'apple green', 'sun red' и т.д.

enum Fruit {
    case apple(String)
    case banana(String)
    case orange(String)
}

func printFruits() {
    let fruits: [Fruit] = [.apple("green"), .banana("yellow"), .orange("orange")]
    for fruit in fruits {
        switch fruit {
        case .apple(let color):
            print("apple \(color)")
        case .banana(let color):
            print("banana \(color)")
        case .orange(let color):
            print("orange \(color)")
        }
    }
}

printFruits()

// ----

// Создать функцию, которая выставляет оценки ученикам в школе, на входе принимает значение enum Score: String {<Допишите case'ы} и выводит числовое значение оценки

enum Score: String {
  case five = "Отлично"
  case four = "Хорошо"
  case three = "Удовлетворительно"
  case two = "Неудовлетворительно"
}

func getStudentScore(_ score: Score) -> Int {
    switch score {
    case .five:
      return 5
    case .four:
      return 4
    case .three:
      return 3
    case .two:
      return 2
  }
}

print(getStudentScore(Score.five))

// ----

// Создать метод, которая выводит в консоль какие автомобили стоят в гараже, используйте enum

enum Car: String {
    case sedan
    case suv
    case sports
    case pickup
}

var garage: [Car] = [.sedan, .suv, .pickup, .pickup]

func garageCars() {
  print("Автомобили в гараже:")
  for car in garage {
      print("- \(car.rawValue)")
  }        
}

garageCars()
